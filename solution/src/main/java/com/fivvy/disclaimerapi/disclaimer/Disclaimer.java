package com.fivvy.disclaimerapi.disclaimer;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document
public class Disclaimer {

    @Id
    private String id;
    private String name;
    private String text;
    private String version;
    private LocalDateTime create_at;
    private LocalDateTime update_at;

    public Disclaimer(String name,
                      String text,
                      String version,
                      LocalDateTime create_at,
                      LocalDateTime update_at) {

        this.name = name;
        this.text = text;
        this.version = version;
        this.create_at = create_at;
        this.update_at = update_at;
    }
}
