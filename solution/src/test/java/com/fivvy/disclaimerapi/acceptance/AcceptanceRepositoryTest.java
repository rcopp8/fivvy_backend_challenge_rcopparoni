package com.fivvy.disclaimerapi.acceptance;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class AcceptanceRepositoryTest {

    @Autowired
    private AcceptanceRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void ItShouldFindAcceptancesByUserId(){

        //preparation
        String user_id = "00000001";
        Acceptance acceptance = new Acceptance(
                "64a430933600ec1f968a0eee",
                user_id,
                LocalDateTime.now()
        );
        underTest.save(acceptance);

        //execution
        Optional<List<Acceptance>> results = underTest.findAcceptancesByUserId(user_id);
        String expected = (results.isPresent() && !results.get().isEmpty())? results.get().get(0).getUser_id() : "";

        //validation
        Assert.isTrue(expected.equals(user_id), "Success");
    }

    @Test
    void ItShouldNotFindAcceptancesByUserId(){

        //preparation
        String user_id = "00000001";

        //execution
        Optional<List<Acceptance>> results = underTest.findAcceptancesByUserId(user_id);
        String expected = (results.isPresent() && !results.get().isEmpty())? results.get().get(0).getUser_id() : "";

        //validation
        Assert.isTrue(!expected.equals(user_id), "Success");
    }

    @Test
    void ItShouldFindAcceptanceByDisclaimerAndUserIds(){

        //preparation
        String disclaimer_id = "64a430933600ec1f968a0eee";
        String user_id = "00000001";
        Acceptance acceptance = new Acceptance(
                disclaimer_id,
                user_id,
                LocalDateTime.now()
        );
        underTest.save(acceptance);

        //execution
        Optional<Acceptance> result = underTest.findAcceptanceByDisclaimerAndUserIds(disclaimer_id, user_id);
        String expected = result.isPresent()? result.get().getDisclaimer_id() : "";

        //validation
        Assert.isTrue(expected.equals(disclaimer_id), "Success");
    }

    @Test
    void ItShouldNotFindAcceptanceByDisclaimerAndUserIds(){

        //preparation
        String disclaimer_id = "64a430933600ec1f968a0eee";
        String user_id = "00000001";

        //execution
        Optional<Acceptance> result = underTest.findAcceptanceByDisclaimerAndUserIds(disclaimer_id, user_id);
        String expected = result.isPresent()? result.get().getDisclaimer_id() : "";

        //validation
        Assert.isTrue(!expected.equals(disclaimer_id), "Success");
    }
}