package com.fivvy.disclaimerapi.disclaimer;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DisclaimerRepository extends MongoRepository<Disclaimer, String> {

    //Find disclaimers by text - Method named query
    Optional<List<Disclaimer>> findDisclaimersByText(String text);

    //Find disclaimers by name - Method named query
    Optional<Disclaimer> findDisclaimerByName(String name);
}
