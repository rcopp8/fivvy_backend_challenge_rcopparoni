package com.fivvy.disclaimerapi.acceptance;

import com.fivvy.disclaimerapi.disclaimer.Disclaimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/acceptance")
public class AcceptanceController{

    private final AcceptanceService acceptanceService;

    @Autowired
    public AcceptanceController(AcceptanceService acceptanceService){this.acceptanceService = acceptanceService;}

    @GetMapping
    public List<Acceptance> getAcceptances(@RequestParam(required = false) String user_id){

        return acceptanceService.getAcceptances(user_id);
    }

    @PostMapping
    public void addAcceptance(@RequestBody Acceptance acceptance){acceptanceService.addAcceptance(acceptance);}
}
