package com.fivvy.disclaimerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DisclaimerAcceptanceApplication {

	public static void main(String[] args) {

		SpringApplication.run(DisclaimerAcceptanceApplication.class, args);
	}
}
