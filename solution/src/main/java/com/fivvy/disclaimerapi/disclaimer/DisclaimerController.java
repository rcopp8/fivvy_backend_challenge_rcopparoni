package com.fivvy.disclaimerapi.disclaimer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/disclaimer")
public class DisclaimerController {

    private final DisclaimerService disclaimerService;

    @Autowired
    public DisclaimerController(DisclaimerService disclaimerService){this.disclaimerService = disclaimerService;}

    @GetMapping
    public List<Disclaimer> getDisclaimers(@RequestParam(required = false) String text){

        return disclaimerService.getDisclaimers(text);
    }

    @PostMapping
    public void addDisclaimer(@RequestBody Disclaimer disclaimer){disclaimerService.addDisclaimer(disclaimer);}

    @DeleteMapping(path = "{disclaimer_id}")
    public void deleteDisclaimer(@PathVariable("disclaimer_id") String id){

        disclaimerService.deleteDisclaimer(id);
    }

    @PutMapping(path = "{disclaimer_id}")
    public void updateCar(
            @PathVariable("disclaimer_id") String id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String text,
            @RequestParam(required = false) String version){

        disclaimerService.updateDisclaimer(id, name, text, version);
    }

}
