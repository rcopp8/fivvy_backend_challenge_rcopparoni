package com.fivvy.disclaimerapi.acceptance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@Service
public class AcceptanceService {

    private final AcceptanceRepository acceptanceRepository;

    @Autowired
    public AcceptanceService(AcceptanceRepository acceptanceRepository){

        this.acceptanceRepository = acceptanceRepository;
    }

    public List<Acceptance> getAcceptances(String user_id) {

        if (user_id != null && user_id.length() > 0){
            Optional<List<Acceptance>> acceptancesByUserId = acceptanceRepository.findAcceptancesByUserId(user_id);

            //If there aren't any acceptances from the specified user id, the result is an empty list.
            if (acceptancesByUserId.isPresent()){
                return acceptancesByUserId.get();
            }
        }
        return acceptanceRepository.findAll();
    }

    public void addAcceptance(Acceptance acceptance){

        Optional<Acceptance> acceptanceByDisclaimerAndUserIds = acceptanceRepository
                .findAcceptanceByDisclaimerAndUserIds(acceptance.getDisclaimer_id(), acceptance.getUser_id());

        //Assuming that two acceptances cannot have the same disclaimer and user ids
        if(acceptanceByDisclaimerAndUserIds.isPresent()){
            throw new HttpClientErrorException(HttpStatusCode.valueOf(409), "Acceptance already exists");
        }
        acceptanceRepository.save(acceptance);
    }
}
