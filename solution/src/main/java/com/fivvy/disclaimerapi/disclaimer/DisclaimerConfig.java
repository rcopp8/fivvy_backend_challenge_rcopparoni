package com.fivvy.disclaimerapi.disclaimer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
public class DisclaimerConfig {

    //Bean for disclaimer example creation to populate database
    @Bean
    CommandLineRunner commandLineRunner(DisclaimerRepository disclaimerRepository){

        return args -> {

            Disclaimer disclaimer = new Disclaimer(
                    "Terms and Conditions",
                    "Lorem ipsum dolor sit amet",
                    "1.0.0",
                    LocalDateTime.now(),
                    LocalDateTime.now()
                    );

            //Uncomment the line below to make effective insertion
            //disclaimerRepository.insert(disclaimer);
        };
    }
}
