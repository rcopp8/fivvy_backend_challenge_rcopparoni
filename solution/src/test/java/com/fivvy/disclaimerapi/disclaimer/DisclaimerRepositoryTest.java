package com.fivvy.disclaimerapi.disclaimer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class DisclaimerRepositoryTest {

    @Autowired
    private DisclaimerRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void ItShouldFindDisclaimersByText(){

        //preparation
        String testText = "Lorem ipsum dolor sit amet";
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                testText,
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        underTest.save(disclaimer);

        //execution
        Optional<List<Disclaimer>> results = underTest.findDisclaimersByText(testText);
        String expected = (results.isPresent() && !results.get().isEmpty())? results.get().get(0).getText() : "";

        //validation
        Assert.isTrue(expected.equals(testText), "Success");
    }

    @Test
    void ItShouldNotFindDisclaimersByText(){

        //preparation
        String testText = "Lorem ipsum dolor sit amet";

        //execution
        Optional<List<Disclaimer>> results = underTest.findDisclaimersByText(testText);
        String expected = (results.isPresent() && !results.get().isEmpty())? results.get().get(0).getText() : "";

        //validation
        Assert.isTrue(!expected.equals(testText), "Success");
    }

    @Test
    void ItShouldFindDisclaimerByName(){

        //preparation
        String testName = "Testing disclaimer";
        Disclaimer disclaimer = new Disclaimer(
                testName,
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        underTest.save(disclaimer);

        //execution
        Optional<Disclaimer> result = underTest.findDisclaimerByName(testName);
        String expected = result.isPresent()? result.get().getName() : "";

        //validation
        Assert.isTrue(expected.equals(testName), "Success");
    }

    @Test
    void ItShouldNotFindDisclaimerByName(){

        //preparation
        String testName = "Testing disclaimer";

        //execution
        Optional<Disclaimer> result = underTest.findDisclaimerByName(testName);
        String expected = result.isPresent()? result.get().getName() : "";

        //validation
        Assert.isTrue(!expected.equals(testName), "Success");
    }
}