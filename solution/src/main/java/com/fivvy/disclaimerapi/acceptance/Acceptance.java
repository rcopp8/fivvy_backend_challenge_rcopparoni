package com.fivvy.disclaimerapi.acceptance;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document
public class Acceptance {

    @Id
    private String id;
    private String disclaimer_id;
    private String user_id;
    private LocalDateTime create_at;

    public Acceptance(String disclaimer_id,
                      String user_id,
                      LocalDateTime create_at) {

        this.disclaimer_id = disclaimer_id;
        this.user_id = user_id;
        this.create_at = create_at;
    }
}
