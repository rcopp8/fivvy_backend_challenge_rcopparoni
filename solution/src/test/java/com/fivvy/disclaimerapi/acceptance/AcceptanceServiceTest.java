package com.fivvy.disclaimerapi.acceptance;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class AcceptanceServiceTest {

    @Mock private AcceptanceRepository acceptanceRepository;
    private AcceptanceService underTest;

    @BeforeEach
    void setUp(){
        underTest = new AcceptanceService(acceptanceRepository);
    }

    @Test
    void canGetAcceptancesWithOutUserId() {

        //preparation //execution
        underTest.getAcceptances(null);

        //validation
        verify(acceptanceRepository).findAll();
    }

    @Test
    void canGetAcceptancesWithUserId() {

        //preparation
        String testId = "00000001";

        //execution
        underTest.getAcceptances(testId);
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(acceptanceRepository).findAcceptancesByUserId(stringArgumentCaptor.capture());

        //validation
        String captured = stringArgumentCaptor.getValue();
        Assert.isTrue(captured.equals(testId), "Success");
    }

    @Test
    void canAddAcceptance() {

        //preparation
        Acceptance acceptance = new Acceptance(
                "64a430933600ec1f968a0eee",
                "00000001",
                LocalDateTime.now()
        );

        //execution
        underTest.addAcceptance(acceptance);
        ArgumentCaptor<Acceptance> acceptanceArgumentCaptor = ArgumentCaptor.forClass(Acceptance.class);
        verify(acceptanceRepository).save(acceptanceArgumentCaptor.capture());

        //validation
        Acceptance captured = acceptanceArgumentCaptor.getValue();
        Assert.isTrue(captured.equals(acceptance), "Success");
    }

    @Test
    void canNotAddAcceptanceWhenDisclaimerAndUserIdsAreTaken() {

        //preparation
        Acceptance acceptance = new Acceptance(
                "64a430933600ec1f968a0eee",
                "00000001",
                LocalDateTime.now()
        );
        given(acceptanceRepository.findAcceptanceByDisclaimerAndUserIds(
                acceptance.getDisclaimer_id(),
                acceptance.getUser_id()))
                .willReturn(Optional.of(acceptance));

        //execution //validation
        assertThatThrownBy(() -> underTest.addAcceptance(acceptance))
                .isInstanceOf(HttpClientErrorException.class)
                .hasMessageContaining("Acceptance already exists");

        verify(acceptanceRepository, never()).save(any());
    }
}