package com.fivvy.disclaimerapi.disclaimer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import org.springframework.web.client.HttpClientErrorException;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class DisclaimerServiceTest {

    @Mock private DisclaimerRepository disclaimerRepository;
    private DisclaimerService underTest;

    @BeforeEach
    void setUp(){
        underTest = new DisclaimerService(disclaimerRepository);
    }

    @Test
    void canGetDisclaimersWithOutText() {

        //preparation //execution
        underTest.getDisclaimers(null);

        //validation
        verify(disclaimerRepository).findAll();
    }

    @Test
    void canGetDisclaimersWithText() {

        //preparation
        String testText = "Lorem ipsum dolor sit amet";

        //execution
        underTest.getDisclaimers(testText);
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(disclaimerRepository).findDisclaimersByText(stringArgumentCaptor.capture());

        //validation
        String captured = stringArgumentCaptor.getValue();
        Assert.isTrue(captured.equals(testText), "Success");
    }

    @Test
    void canAddDisclaimer() {

        //preparation
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );

        //execution
        underTest.addDisclaimer(disclaimer);
        ArgumentCaptor<Disclaimer> disclaimerArgumentCaptor = ArgumentCaptor.forClass(Disclaimer.class);
        verify(disclaimerRepository).save(disclaimerArgumentCaptor.capture());

        //validation
        Disclaimer captured = disclaimerArgumentCaptor.getValue();
        Assert.isTrue(captured.equals(disclaimer), "Success");
    }

    @Test
    void canNotAddDisclaimerWhenNameIsTaken() {

        //preparation
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        given(disclaimerRepository.findDisclaimerByName(disclaimer.getName())).willReturn(Optional.of(disclaimer));

        //execution //validation
        assertThatThrownBy(() -> underTest.addDisclaimer(disclaimer))
                .isInstanceOf(HttpClientErrorException.class)
                .hasMessageContaining("Disclaimer already exists");

        verify(disclaimerRepository, never()).save(any());
    }

    @Test
    void canDeleteDisclaimer() {

        //preparation
        String testId = "64a430933600ec1f968a0eee";
        given(disclaimerRepository.existsById(testId)).willReturn(true);

        //execution
        underTest.deleteDisclaimer(testId);

        //validation
        verify(disclaimerRepository).deleteById(testId);
    }

    @Test
    void canNotDeleteDisclaimerWhenDoesNotExists() {

        //preparation
        String testId = "64a430933600ec1f968a0eee";
        given(disclaimerRepository.existsById(testId)).willReturn(false);

        //execution //validation
        assertThatThrownBy(() -> underTest.deleteDisclaimer(testId))
                .isInstanceOf(HttpClientErrorException.class)
                .hasMessageContaining("Disclaimer does not exists");

        verify(disclaimerRepository, never()).deleteById(any());
    }

    @Test
    void canNotUpdateDisclaimerWithOutFields() {

        //preparation
        String testId = "64a430933600ec1f968a0eee";
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        given(disclaimerRepository.findById(testId)).willReturn(Optional.of(disclaimer));

        //execution
        underTest.updateDisclaimer(testId, null, null, null);

        //validation
        verify(disclaimerRepository, never()).save(any());
    }

    @Test
    void canNotUpdateDisclaimerWhenDoesNotExists(){

        //preparation
        String testId = "64a430933600ec1f968a0eee";
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        given(disclaimerRepository.findById(testId)).willReturn(Optional.empty());

        //execution //validation
        assertThatThrownBy(() -> underTest.updateDisclaimer(testId, null, null, null))
                .isInstanceOf(HttpClientErrorException.class)
                .hasMessageContaining("Disclaimer does not exists");

        verify(disclaimerRepository, never()).save(any());
    }

    @Test
    void canUpdateDisclaimerName(){

        //preparation
        String testId = "64a430933600ec1f968a0eee";
        String testName = "Testing disclaimer update";
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        given(disclaimerRepository.findById(testId)).willReturn(Optional.of(disclaimer));
        given(disclaimerRepository.findDisclaimerByName(testName)).willReturn(Optional.empty());

        //execution
        underTest.updateDisclaimer(testId, testName, null, null);

        //validation
        verify(disclaimerRepository).save(any());
    }

    @Test
    void canNotUpdateDisclaimerWhenNameTaken() {

        //preparation
        String testId = "64a430933600ec1f968a0eee";
        String testName = "Testing disclaimer update";
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        given(disclaimerRepository.findById(testId)).willReturn(Optional.of(disclaimer));
        given(disclaimerRepository.findDisclaimerByName(testName)).willReturn(Optional.of(disclaimer));

        //execution //validation
        assertThatThrownBy(() -> underTest.updateDisclaimer(testId, testName, null, null))
                .isInstanceOf(HttpClientErrorException.class)
                .hasMessageContaining("Disclaimer name already exists");

        verify(disclaimerRepository, never()).save(any());
    }

    @Test
    void canUpdateDisclaimerText(){

        //preparation
        String testId = "64a430933600ec1f968a0eee";
        String testText = "Updated text";
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        given(disclaimerRepository.findById(testId)).willReturn(Optional.of(disclaimer));

        //execution
        underTest.updateDisclaimer(testId, null, testText, null);

        //validation
        verify(disclaimerRepository).save(any());
    }

    @Test
    void canUpdateDisclaimerVersion(){

        //preparation
        String testId = "64a430933600ec1f968a0eee";
        String testVersion = "1.0.1";
        Disclaimer disclaimer = new Disclaimer(
                "Testing disclaimer",
                "Lorem ipsum dolor sit amet",
                "1.0.0",
                LocalDateTime.now(),
                LocalDateTime.now()
        );
        given(disclaimerRepository.findById(testId)).willReturn(Optional.of(disclaimer));

        //execution
        underTest.updateDisclaimer(testId, null, null, testVersion);

        //validation
        verify(disclaimerRepository).save(any());
    }
}