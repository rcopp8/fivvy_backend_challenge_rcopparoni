# Fivvy Backend Challenge - Disclaimer acceptance API

## Springboot REST API with Java

This project was developed in order to satisfy the requirements specified for the "Disclaimer acceptance API" backend coding challenge from Fivvy.

The project is intended to response to API calls that request and manage information regarding different disclaimers and their linked acceptances according to specific users.

For further information see [Fivvy - Disclaimer acceptance API requirements](https://gitlab.com/rcopp8/fivvy_backend_challenge_rcopparoni/-/blob/main/README.md?ref_type=heads).

### Endpoints

    Base URI: http://<HOST>/api/v1

#### Disclaimer
    
    GET | List Disclaimers
    http://<HOST>/api/v1/disclaimer
    Query params: "text":String (optional)

    POST | Create Disclaimer
    http://<HOST>/api/v1/disclaimer
    Request body: "Disclaimer":Object (required)

    DELETE | Delete Disclaimer
    http://<HOST>/api/v1/disclaimer/{disclaimer_id}

    PUT | Update Disclaimer
    http://<HOST>/api/v1/disclaimer/{disclaimer_id}
    Query params: "name":String (optional), "text":String (optional), "version":String (optional)

#### Acceptance

    GET | List Acceptances
    http://<HOST>/api/v1/acceptance
    Query params: "user_id":String (optional)

    POST | Create Acceptance
    http://<HOST>/api/v1/acceptance
    Request body: "Acceptance":Object (required)

## Technologies

### Tools

    * Apache Maven 3.8.7
    * Docker 24.0.2
    * Java 17.0.6 LTS
    * MongoDB 6.0.7
    * Springboot 3.1.0

### Springboot dependencies

    * AssertJ 3.24.2
    * Flapdoodle 4.7.0
    * Lombok 1.18.28
    * Spring Data Mongo 3.1.1    
    * Spring Web 3.1.0

## Project setup

For local project testing, ensure to have all the following steps completed before executing the application:

    1. Set up the environment with all tools specified above, it is recommended to respect the versions.
    2. Run docker-compose.yaml* file in order to start the MongoDB container.
    3. Run the DisclaimerAcceptanceApplication.java file.

#### *Docker compose file can be found [here](https://gitlab.com/rcopp8/fivvy_backend_challenge_rcopparoni/-/blob/main/solution/docker-compose.yaml?ref_type=heads).

## Delivery

#### As per [Fivvy - Disclaimer acceptance API requirements](https://gitlab.com/rcopp8/fivvy_backend_challenge_rcopparoni/-/blob/main/README.md?ref_type=heads), Delivery conditions for the solution proposal could not be met since original repository "fork" action is not allowed without permission.

## Extras

* Postman Collection to test the application endpoints [Fivvy - Postman](https://gitlab.com/rcopp8/fivvy_backend_challenge_rcopparoni/-/blob/main/solution/extras/Fivvy.postman_collection.json?ref_type=heads).
* Java .jar file [disclaimer-api-1.0.0.jar](https://gitlab.com/rcopp8/fivvy_backend_challenge_rcopparoni/-/blob/main/solution/extras/disclaimer-api-1.0.0.jar?ref_type=heads).
* Aditional useful software for testing: MongoDB Compass 1.36.2
