package com.fivvy.disclaimerapi.acceptance;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AcceptanceRepository extends MongoRepository<Acceptance, String> {

    @Query("{'user_id' : ?0}")
    Optional<List<Acceptance>> findAcceptancesByUserId(String user_id);

    @Query("{'disclaimer_id' : ?0, 'user_id' : ?1}")
    Optional<Acceptance> findAcceptanceByDisclaimerAndUserIds(String disclaimer_id, String user_id);
}
