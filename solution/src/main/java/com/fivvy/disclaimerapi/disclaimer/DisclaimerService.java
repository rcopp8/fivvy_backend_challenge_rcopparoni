package com.fivvy.disclaimerapi.disclaimer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class DisclaimerService {

    private final DisclaimerRepository disclaimerRepository;

    @Autowired
    public DisclaimerService(DisclaimerRepository disclaimerRepository){

        this.disclaimerRepository = disclaimerRepository;
    }

    public List<Disclaimer> getDisclaimers(String text){

        if (text != null && text.length() > 0){
            Optional<List<Disclaimer>> disclaimersByText = disclaimerRepository.findDisclaimersByText(decodeURL(text));

            //If there aren't any disclaimers with the specified text, the result is an empty list.
            if (disclaimersByText.isPresent()){
                return disclaimersByText.get();
            }
        }
        return disclaimerRepository.findAll();
    }

    public void addDisclaimer(Disclaimer disclaimer){

        Optional<Disclaimer> disclaimerByName = disclaimerRepository.findDisclaimerByName(disclaimer.getName());

        //Assuming that two disclaimers cannot have the same name
        if(disclaimerByName.isPresent()){
            throw new HttpClientErrorException(HttpStatusCode.valueOf(409), "Disclaimer already exists");
        }
        disclaimerRepository.save(disclaimer);
    }

    public void deleteDisclaimer(String id){

        boolean exists = disclaimerRepository.existsById(id);
        if(!exists){
            throw new HttpClientErrorException(HttpStatusCode.valueOf(404), "Disclaimer does not exists");
        }
        disclaimerRepository.deleteById(id);
    }

    @Transactional
    public void updateDisclaimer(String id, String name, String text, String version){

        Disclaimer disclaimer = disclaimerRepository.findById(id)
                .orElseThrow(() -> new HttpClientErrorException(
                        HttpStatusCode.valueOf(404), "Disclaimer does not exists"
                ));

        //Update flag
        boolean updated = false;

        //Name update
        if (name != null){
            String decodedName = decodeURL(name);
            if (decodedName.length() > 0 && !Objects.equals(disclaimer.getName(), decodedName)) {
                Optional<Disclaimer> disclaimerByName = disclaimerRepository.findDisclaimerByName(decodedName);
                if (disclaimerByName.isPresent()) {
                    throw new HttpClientErrorException(
                            HttpStatusCode.valueOf(409), "Disclaimer name already exists"
                    );
                }
                disclaimer.setName(decodedName);
                updated = true;
            }
        }

        //Text update
        if (text != null){
            String decodedText = decodeURL(text);
            if (decodedText.length() > 0 && !Objects.equals(disclaimer.getText(), decodedText)) {
                disclaimer.setText(decodedText);
                updated = true;
            }
        }

        //Version update
        if (version != null && version.length() > 0 && !Objects.equals(disclaimer.getVersion(), version)) {
            if(checkSemVer(version)){
                disclaimer.setVersion(version);
                updated = true;
            } else throw new HttpClientErrorException(HttpStatusCode.valueOf(400), "Invalid version format");
        }

        //Latest update
        if (updated){
            disclaimer.setUpdate_at(LocalDateTime.now());
            disclaimerRepository.save(disclaimer);
        }
    }

    private static String decodeURL(String encoded){return URLDecoder.decode(encoded, StandardCharsets.UTF_8);}

    private static Boolean checkSemVer(String version){

        Pattern versionPattern = Pattern.compile("[1-9]\\d*\\.\\d+\\.\\d+(?:-[a-zA-Z0-9]+)?");
        Matcher matcher = versionPattern.matcher(version);
        return matcher.matches();
    }
}
